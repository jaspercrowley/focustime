import React, { Component } from 'react'
import './App.css'
import ThemeProvider from './contexts/ThemeContext'
import Home from './containers/Home'
import MinimizeButton from './components/MinimizeButton'
import CloseButton from './components/CloseButton'
import OptionsButton from './components/OptionsButton'
import Options from './containers/Options'

const electron = window.require('electron')

class App extends Component {
  state = {
    optionsEnabled: false,
  }

  showOptions = () => {
    console.log('showOptions!')
    this.setState(state => ({ optionsEnabled: !state.optionsEnabled }))
  }

  onMinimize = () => {
    electron.remote.getCurrentWindow().minimize()
  }

  onClose = () => {

  }

  render() {
    const { optionsEnabled } = this.state
    const { onClose, onMinimize, showOptions } = this
    return (
      <ThemeProvider>
        <div className="App">
          <OptionsButton active={optionsEnabled} showOptions={showOptions} />
          <MinimizeButton minimize={onMinimize} />
          <CloseButton close={onClose} />
          <Home />
          {optionsEnabled ? <Options showOptions={showOptions} /> : ''}
        </div>
      </ThemeProvider>
    )
  }
}
export default App
