import React, { Component } from 'react'

const ThemeContext = React.createContext()
export const ThemeConsumer = ThemeContext.Consumer

const themes = {
  normal: '#651FFF',
  break: '#00e676',
}

class ThemeProvider extends Component {
  state = {
    currentTheme: themes.normal,
  }

  changeTheme = (theme) => { this.setState(state => ({ currentTheme: theme })) }

  render() {
    const { currentTheme } = this.state
    const { children } = this.props
    return (
      <ThemeContext.Provider value={{
        currentTheme,
        changeTheme: this.changeTheme,
        themes,
      }}
      >
        {children}
      </ThemeContext.Provider>
    )
  }
}

export default ThemeProvider
