import React, { Component } from 'react'
import settings from '../settings'

const TimerContext = React.createContext()
export const TimerConsumer = TimerContext.Consumer

class TimerProvider extends Component {
  constructor(props) {
    super(props)
    this.timer = null
  }

state = settings

componentDidMount() {
  try {
    const workSong = new Audio(settings.workSong)
    const breakSong = new Audio(settings.breakSong)
    this.setState(state => ({ breakSong, workSong }))
  } catch (error) {
    console.log(error)
  }
}

onTimerStart = () => {
  this.timer = setInterval(this.onTick, 1000)
  this.setState(state => ({ timerStarted: true }))
}

onTimerPause = () => {
  clearInterval(this.timer)
  this.setState(state => ({ timerStarted: false }))
}

onTimerReset = () => {
  clearInterval(this.timer)
  this.setState(state => ({
    timerStarted: false,
    working: true,
    currentRound: 1,
    breakTime: 300,
    currentTime: state.workTime,
  }))
  this.setTheme(true)
}

onTimerSkip = () => {
  this.setUpRound()
}

onTick = () => {
  const {
    currentTime, working, workSong, breakSong, volume,
  } = this.state
  const time = currentTime - 1
  if (time <= 0) {
    this.setUpRound()
    if (working) {
      breakSong.volume = volume / 100
      breakSong.play()
    } else {
      workSong.volume = volume / 100
      workSong.play()
    }
  } else this.setState(state => ({ currentTime: time }))
}

setVolume = () => {
  this.setState((state) => {
    switch (state.volume) {
      case 0: return { volume: 33, volumeIcon: 'Low' }
      case 33: return { volume: 66, volumeIcon: 'Medium' }
      case 66: return { volume: 100, volumeIcon: 'High' }
      default: return { volume: 0, volumeIcon: 'Mute' }
    }
  })
}

setUpRound = () => {
  const {
    currentRound, maxRounds, working, timerStarted,
  } = this.state
  this.setTheme()
  if (currentRound >= maxRounds && working === false) {
    this.onTimerReset()
    if (timerStarted) this.onTimerStart()
  } else {
    this.setState((state) => {
      const time = this.getTime()
      return ({
        working: !working,
        currentTime: time,
        currentRound: !working ? currentRound + 1 : currentRound,
      })
    })
  }
}

getTime = () => {
  const {
    workTime, breakTime, longBreakTime, working, currentRound, maxRounds,
  } = this.state
  if (!working) return workTime
  if (currentRound === maxRounds) return longBreakTime
  return breakTime
}

setTheme = (reset = false) => {
  const { working } = this.state
  const { changeTheme, themes } = this.props

  if (!reset) {
    changeTheme(working ? themes.break : themes.normal)
  } else changeTheme(themes.normal)
}

render() {
  const {
    currentTime, working, workTime, breakTime, timerStarted, maxRounds, currentRound, volumeIcon,
  } = this.state
  const {
    onTimerStart, onTimerPause, onTimerReset, onTimerSkip, setVolume,
  } = this
  const { children } = this.props
  return (
    <TimerContext.Provider
      value={{
        currentTime,
        maxTime: working ? workTime : breakTime,
        timerStarted,
        working,
        onTimerStart,
        onTimerPause,
        onTimerReset,
        onTimerSkip,
        setVolume,
        maxRounds,
        currentRound,
        volumeIcon,
      }}
    >
      {children}
    </TimerContext.Provider>
  )
}
}

export default TimerProvider
