import React from 'react'
import './index.css'

const Options = props => (
  <div className="options">
    <div className="options-bg" />
    <button onClick={props.showOptions} className="options-apply-button" type="button">
      Apply
    </button>
  </div>
)

export default Options
