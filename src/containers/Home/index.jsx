import React from 'react'
import Timer from '../../components/Timer'
import './index.css'
import TimerProvider from '../../contexts/TimerContext'
import { ThemeConsumer } from '../../contexts/ThemeContext'

const Home = () => (
  <ThemeConsumer>
    {({ currentTheme, changeTheme, themes }) => (
      <div className="Home">
        <TimerProvider changeTheme={changeTheme} themes={themes}>
          <Timer currentTheme={currentTheme} />
        </TimerProvider>
      </div>
    )}
  </ThemeConsumer>
)

export default Home
