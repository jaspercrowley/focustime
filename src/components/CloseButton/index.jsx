import React from 'react'
import { Close } from '@material-ui/icons'
import './index.css'

const CloseButton = props => (
  <button onClick={props.close} className="close-button" type="button">
    <Close />
  </button>
)

export default CloseButton
