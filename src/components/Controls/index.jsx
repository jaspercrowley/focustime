import React from 'react'
import {
  PlayArrow, Pause, SkipNext, Replay,
} from '@material-ui/icons'
import ControlButton from '../ControlButton'
import VolumeControl from '../VolumeControl'
import './index.css'

const Controls = (props) => {
  const {
    timerStarted, onTimerStart, onTimerPause, onTimerReset, onTimerSkip, setVolume, volumeIcon, currentTheme,
  } = props
  return (
    <div className="controls">
      {
        !timerStarted
          ? <ControlButton color={currentTheme} className="play-button" click={onTimerStart} icon={<PlayArrow fontSize="inherit" />} />
          : <ControlButton color={currentTheme} className="pause-button" click={onTimerPause} icon={<Pause fontSize="inherit" />} />
      }
      <ControlButton color={currentTheme} className="skip-button" click={onTimerSkip} icon={<SkipNext fontSize="inherit" />} />
      <ControlButton color={currentTheme} className="reset-button" click={onTimerReset} icon={<Replay />} />
      <VolumeControl volumeIcon={volumeIcon} setVolume={setVolume} />
    </div>
  )
}

export default Controls
