import React from 'react'
import {
  VolumeUp, VolumeDown, VolumeMute, VolumeOff,
} from '@material-ui/icons'
import './index.css'

const VolumeControl = (props) => {
  const { setVolume, volumeIcon, color } = props
  const Icon = () => {
    switch (volumeIcon) {
      case 'High': return <VolumeUp />
      case 'Medium': return <VolumeDown />
      case 'Low': return <VolumeMute />
      default: return <VolumeOff />
    }
  }
  return (
    <button style={{ color }} onClick={setVolume} className="volume-control" type="button">
      { <Icon />}
    </button>
  )
}
export default VolumeControl
