import React from 'react'
import { Menu } from '@material-ui/icons'
import './index.css'

const OptionsButton = (props) => {
  const { showOptions, active } = props
  return (
    <button type="button" onClick={showOptions} className={`${active ? 'active' : ''} options-button`}>
      <Menu />
    </button>
  )
}
export default OptionsButton
