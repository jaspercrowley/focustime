import React from 'react'
import Controls from '../Controls'
import CircleProgressBar from '../CircleProgressBar'
import './index.css'
import { TimerConsumer } from '../../contexts/TimerContext'

const Timer = props => (
  <TimerConsumer>
    {({
      currentTime,
      maxTime,
      working,
      timerStarted,
      onTimerStart,
      onTimerPause,
      onTimerReset,
      onTimerSkip,
      maxRounds,
      currentRound,
      setVolume,
      volumeIcon,
    }) => (
      <div className="timer">
        <CircleProgressBar
          currentTime={currentTime}
          maxTime={maxTime}
          working={working}
          maxRounds={maxRounds}
          currentRound={currentRound}
          currentTheme={props.currentTheme}
        />
        <Controls
          working={working}
          timerStarted={timerStarted}
          onTimerStart={onTimerStart}
          onTimerPause={onTimerPause}
          onTimerReset={onTimerReset}
          onTimerSkip={onTimerSkip}
          currentTheme={props.currentTheme}
          setVolume={setVolume}
          volumeIcon={volumeIcon}
        />
      </div>
    )}
  </TimerConsumer>
)

export default Timer
