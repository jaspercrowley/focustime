import React from 'react'
import CircularProgressbar from 'react-circular-progressbar'
import './index.css'
import 'react-circular-progressbar/dist/styles.css'

const CircleProgressBar = (props) => {
  const showTime = () => {
    const { currentTime } = props
    const minutes = Math.floor(currentTime / 60)
    const seconds = currentTime % 60
    const timerMinutes = minutes < 10 ? `0${minutes}` : minutes
    const timerSeconds = seconds < 10 ? `0${seconds}` : seconds
    const time = `${timerMinutes}:${timerSeconds}`
    return time
  }

  const showRounds = () => {
    const { maxRounds, currentRound, currentTheme } = props
    let status = { className: '', color: '' }
    const rounds = Array.from(new Array(maxRounds), (val, i) => i + 1)
    return rounds.map((round) => {
      if (round === currentRound) status = { className: 'active', color: currentTheme }
      else if (round < currentRound) status = { className: 'done', color: currentTheme }
      else status = ''
      return <div style={{ background: status.color }} key={round} className={status.className} />
    })
  }

  const {
    currentTime, maxTime, working, currentTheme,
  } = props

  return (
    <div className="circle-progress-bar">
      <span className="rounds">
        { showRounds() }
      </span>
      <CircularProgressbar
        className="progress-circle"
        strokeWidth={2}
        percentage={Math.floor(currentTime / maxTime * 100)}
        text={showTime()}
        backgroundPadding={20}
        initialAnimation="true"
        styles={{
          path: { stroke: currentTheme },
          text: { fill: currentTheme, fontSize: '16px' },
        }}
      />
      <span style={{ color: currentTheme }} className="task-name">
        { working ? 'Work' : 'Break' }
      </span>
    </div>
  )
}

export default CircleProgressBar
