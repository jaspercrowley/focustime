import React from 'react'
import { Remove } from '@material-ui/icons'
import './index.css'

const MinimizeButton = props => (
  <button onClick={props.minimize} type="button" className="minimize-button">
    <Remove />
  </button>
)

export default MinimizeButton
