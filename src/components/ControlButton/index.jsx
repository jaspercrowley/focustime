import React from 'react'
import './index.css'

const ControlButton = (props) => {
  const {
    click, icon, className, color,
  } = props
  return (
    <button style={{ color }} type="button" onClick={click} className={`${className} animated control-button`}>
      {icon}
    </button>
  )
}

export default ControlButton
