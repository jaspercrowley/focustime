const electron = require('electron')

const { app, BrowserWindow } = electron

const path = require('path')
const url = require('url')
const isDev = require('electron-is-dev')

let mainWindow

function createWindow() {
  mainWindow = new BrowserWindow({
    frame: false,
    alwaysOnTop: true,
    fullscreenable: false,
    resizable: false,
    maximizable: false,
    minimizable: true,
    useContentSize: true,
  })
  mainWindow.setSize(280, 300, true)
  mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`)
  mainWindow.on('closed', () => { mainWindow = null })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
